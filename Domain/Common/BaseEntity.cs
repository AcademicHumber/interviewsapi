﻿namespace Domain.Common
{
	/// <summary>
	/// Parent class for all the entities.
	/// </summary>
	public abstract class BaseEntity
	{
		/// <summary>
		/// Gets or sets the id of the entity.
		/// </summary>
		public int Id { get; set; }
	}
}
