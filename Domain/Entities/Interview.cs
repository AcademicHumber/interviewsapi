﻿namespace Domain.Entities
{
	using System;
	using Domain.Common;

	/// <summary>
	/// The interview object class.
	/// </summary>
	public class Interview : BaseEntity
	{
		/// <summary>
		/// Gets or sets the interview name.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the interview description.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the interview StartDate.
		/// </summary>
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Gets or sets the interview Duration.
		/// </summary>
		public int Duration { get; set; }
	}
}
