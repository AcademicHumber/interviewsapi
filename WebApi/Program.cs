namespace WebApi
{
	using Microsoft.AspNetCore.Hosting;
	using Microsoft.Extensions.Hosting;

	/// <summary>
	/// Main program of the API.
	/// </summary>
	public class Program
	{
		/// <summary>
		/// Main method of the API.
		/// </summary>
		/// <param name="args">Args to initialize the program.</param>
		public static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		/// <summary>
		/// Creates a Host.
		/// </summary>
		/// <param name="args">Args to initialize the host.</param>
		/// <returns>A built host.</returns>
		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
