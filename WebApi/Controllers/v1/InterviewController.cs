﻿namespace WebApi.Controllers.V1
{
	using System.Threading.Tasks;
	using Application.Features.InterviewFeatures.Commands;
	using Application.Features.InterviewFeatures.Queries;
	using MediatR;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.Extensions.DependencyInjection;

	/// <summary>
	/// Interviews controller.
	/// </summary>
	[ApiVersion("1.0")]
	public class InterviewController : BaseApiController
	{
		/// <summary>
		/// Creates a new interview.
		/// </summary>
		/// <param name="command">The command</param>
		/// <returns>Interview Id.</returns>
		[HttpPost("")]
		public async Task<IActionResult> Create(CreateInterviewCommand command)
		{
			return this.Ok(await this.Mediator.Send(command));
		}

		/// <summary>
		/// Gets all interviews.
		/// </summary>
		/// <returns>Array with all the interviews registered.</returns>
		[HttpGet("")]
		public async Task<IActionResult> GetAll()
		{
			return this.Ok(await this.Mediator.Send(new GetAllInterviewsQuery()));
		}

		/// <summary>
		/// Gets an interview by Id.
		/// </summary>
		/// <param name="id">Interview Id.</param>
		/// <returns>The required interview.</returns>
		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(int id)
		{
			return this.Ok(await this.Mediator.Send(new GetInterviewByIdQuery { Id = id }));
		}

		/// <summary>
		/// Deletes an interview by Id.
		/// </summary>
		/// <param name="id">Interview Id.</param>
		/// <returns>The deleted interview id.</returns>
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			return this.Ok(await this.Mediator.Send(new DeleteInterviewByIdCommand { Id = id }));
		}

		/// <summary>
		/// Updates an interview by Id.
		/// </summary>
		/// <param name="id">The interview Id.</param>
		/// <param name="command">Command.</param>
		/// <returns>The updated interview Id.</returns>
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(int id, UpdateInterviewCommand command)
		{
			if (id != command.Id)
			{
				return this.BadRequest();
			}

			return this.Ok(await this.Mediator.Send(command));
		}
	}
}
