﻿namespace WebApi.Controllers
{
	using MediatR;
	using Microsoft.AspNetCore.Http;
	using Microsoft.AspNetCore.Mvc;
	using Microsoft.Extensions.DependencyInjection;

	/// <summary>
	/// Base Api controller to version APIs.
	/// </summary>
	[ApiController]
	[Route("api/v{version:apiVersion}/[controller]")]
	public abstract class BaseApiController : ControllerBase
	{
		/// <summary>
		/// The mediatR object to implement the mediator pattern.
		/// </summary>
		private IMediator _mediator;

		/// <summary>
		/// Gets the Requests handler.
		/// </summary>
		protected IMediator Mediator => this._mediator ??= this.HttpContext.RequestServices.GetService<IMediator>();
	}
}