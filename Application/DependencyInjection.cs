﻿namespace Application
{
	using System.Reflection;
	using MediatR;
	using Microsoft.Extensions.DependencyInjection;

	/// <summary>
	/// Static class to extend the IserviceCollection methods.
	/// </summary>
	public static class DependencyInjection
	{
		/// <summary>
		/// Allows services to add Mediator services in order to keep the code clean.
		/// </summary>
		/// <param name="services">Iservice Collection instance.</param>
		public static void AddApplication(this IServiceCollection services)
		{
			services.AddMediatR(Assembly.GetExecutingAssembly());
		}
	}
}