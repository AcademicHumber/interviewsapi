﻿namespace Application.Features.InterviewFeatures.Queries
{
	using System.Linq;
	using System.Threading;
	using System.Threading.Tasks;
	using Application.Interfaces;
	using Domain.Entities;
	using MediatR;

	/// <summary>
	/// Main class to get interviews by Id.
	/// </summary>
	public class GetInterviewByIdQuery : IRequest<Interview>
	{
		/// <summary>
		/// Gets or Sets the Id of the interview.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Handler to get interviews by Id.
		/// </summary>
		public class GetInterviewByIdQueryHandler : IRequestHandler<GetInterviewByIdQuery, Interview>
		{
			private readonly IApplicationDbContext _context;

			/// <summary>
			/// Initializes a new instance of the <see cref="GetInterviewByIdQueryHandler"/> class.
			/// </summary>
			/// <param name="context">The db context.</param>
			public GetInterviewByIdQueryHandler(IApplicationDbContext context)
			{
				this._context = context;
			}

			/// <summary>
			/// The handler method of the class.
			/// </summary>
			/// <param name="query">Query.</param>
			/// <param name="cancellationToken">The token to cancel the operation.</param>
			/// <returns>The requested interview.</returns>
			public async Task<Interview> Handle(GetInterviewByIdQuery query, CancellationToken cancellationToken)
			{
				var interview = this._context.Interviews.Where(a => a.Id == query.Id).FirstOrDefault();
				if (interview == null)
				{
					return null;
				}

				return interview;
			}
		}
	}
}
