﻿namespace Application.Features.InterviewFeatures.Queries
{
	using System.Collections.Generic;
	using System.Threading;
	using System.Threading.Tasks;
	using Application.Interfaces;
	using Domain.Entities;
	using MediatR;
	using Microsoft.EntityFrameworkCore;

	/// <summary>
	/// Main class to get all the interviews.
	/// </summary>
	public class GetAllInterviewsQuery : IRequest<IEnumerable<Interview>>
	{
		/// <summary>
		/// Handler to get interviews by Id.
		/// </summary>
		public class GetAllInterviewsQueryHandler : IRequestHandler<GetAllInterviewsQuery, IEnumerable<Interview>>
		{
			private readonly IApplicationDbContext _context;

			/// <summary>
			/// Initializes a new instance of the <see cref="GetAllInterviewsQueryHandler"/> class.
			/// </summary>
			/// <param name="context">The db context.</param>
			public GetAllInterviewsQueryHandler(IApplicationDbContext context)
			{
				this._context = context;
			}

			/// <summary>
			/// The handler method of the class.
			/// </summary>
			/// <param name="query">Query.</param>
			/// <param name="cancellationToken">The token to cancel the operation.</param>
			/// <returns>List of interviews.</returns>
			public async Task<IEnumerable<Interview>> Handle(GetAllInterviewsQuery query, CancellationToken cancellationToken)
			{
				var interviewsList = await this._context.Interviews.ToListAsync();
				if (interviewsList == null)
				{
					return null;
				}

				return interviewsList.AsReadOnly();
			}
		}
	}
}
