﻿namespace Application.Features.InterviewFeatures.Commands
{
	using System;
	using System.Threading;
	using System.Threading.Tasks;
	using Application.Interfaces;
	using Domain.Entities;
	using MediatR;

	/// <summary>
	/// Main class to create interviews.
	/// </summary>
	public class CreateInterviewCommand : IRequest<int>
	{
		/// <summary>
		/// Gets or sets the interview Id.
		/// </summary>
		// public int Id { get; set; }

		/// <summary>
		/// Gets or sets the interview name.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the interview Startdate.
		/// </summary>
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Gets or sets the interview name.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the interview duration.
		/// </summary>
		public int Duration { get; set; }

		/// <summary>
		/// Handler of the create interview command.
		/// </summary>
		public class CreateInterviewCommandHandler : IRequestHandler<CreateInterviewCommand, int>
		{
			/// <summary>
			/// The db context.
			/// </summary>
			private readonly IApplicationDbContext _context;

			/// <summary>
			/// Initializes a new instance of the <see cref="CreateInterviewCommandHandler"/> class.
			/// </summary>
			/// <param name="context">The db context.</param>
			public CreateInterviewCommandHandler(IApplicationDbContext context)
			{
				this._context = context;
			}

			/// <summary>
			/// Handler method to create interviews.
			/// </summary>
			/// <param name="command">The command.</param>
			/// <param name="cancellationToken">The token to cancel the operation.</param>
			/// <returns>Id of the new created interview.</returns>
			public async Task<int> Handle(CreateInterviewCommand command, CancellationToken cancellationToken)
			{
				var interview = new Interview();
				interview.Name = command.Name;
				interview.StartDate = command.StartDate;
				interview.Duration = command.Duration;
				this._context.Interviews.Add(interview);
				await this._context.SaveChangesAsync();
				return interview.Id;
			}
		}
	}
}
