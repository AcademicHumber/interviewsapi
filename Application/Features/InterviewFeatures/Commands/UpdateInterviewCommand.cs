﻿namespace Application.Features.InterviewFeatures.Commands
{
	using System;
	using System.Linq;
	using System.Threading;
	using System.Threading.Tasks;
	using Application.Interfaces;
	using MediatR;

	/// <summary>
	/// Main class to update interviews.
	/// </summary>
	public class UpdateInterviewCommand : IRequest<int>
	{
		/// <summary>
		/// Gets or sets the interview Id.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the interview name.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the interview name.
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the interview Startdate.
		/// </summary>
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Gets or sets the interview duration.
		/// </summary>
		public int Duration { get; set; }

		/// <summary>
		/// Command handler to update interviews.
		/// </summary>
		public class UpdateInterviewCommandHandler : IRequestHandler<UpdateInterviewCommand, int>
		{
			/// <summary>
			/// The db context.
			/// </summary>
			private readonly IApplicationDbContext _context;

			/// <summary>
			/// Initializes a new instance of the <see cref="UpdateInterviewCommandHandler"/> class.
			/// </summary>
			/// <param name="context">The db context.</param>
			public UpdateInterviewCommandHandler(IApplicationDbContext context)
			{
				this._context = context;
			}

			/// <summary>
			/// Handler method to update interviews.
			/// </summary>
			/// <param name="command">Command.</param>
			/// <param name="cancellationToken">Token to cancel the operation.</param>
			/// <returns>The id of the updated interview.</returns>
			public async Task<int> Handle(UpdateInterviewCommand command, CancellationToken cancellationToken)
			{
				var interview = this._context.Interviews.Where(a => a.Id == command.Id).FirstOrDefault();

				if (interview == null)
				{
					return default;
				}
				else
				{
					interview.Name = command.Name;
					interview.StartDate = command.StartDate;
					interview.Duration = command.Duration;
					await this._context.SaveChangesAsync();
					return interview.Id;
				}
			}
		}
	}
}
