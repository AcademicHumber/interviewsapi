﻿namespace Application.Features.InterviewFeatures.Commands
{
	using Application.Interfaces;
	using MediatR;
	using Microsoft.EntityFrameworkCore;
	using System.Linq;
	using System.Threading;
	using System.Threading.Tasks;

	/// <summary>
	/// Main class to delete interviews.
	/// </summary>
	public class DeleteInterviewByIdCommand : IRequest<int>
	{
		/// <summary>
		/// Gets or sets the id of the interview.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Command handler to delete interviews.
		/// </summary>
		public class DeleteInterviewByIdCommandHandler : IRequestHandler<DeleteInterviewByIdCommand, int>
		{
			/// <summary>
			/// The db context.
			/// </summary>
			private readonly IApplicationDbContext _context;

			/// <summary>
			/// Initializes a new instance of the <see cref="DeleteInterviewByIdCommandHandler"/> class.
			/// </summary>
			/// <param name="context">The db context.</param>
			public DeleteInterviewByIdCommandHandler(IApplicationDbContext context)
			{
				this._context = context;
			}

			/// <summary>
			/// Handler method to delete interviews.
			/// </summary>
			/// <param name="command">Command.</param>
			/// <param name="cancellationToken">The token to cancel the operation.</param>
			/// <returns>The id of the deleted interview.</returns>
			public async Task<int> Handle(DeleteInterviewByIdCommand command, CancellationToken cancellationToken)
			{
				var interview = await this._context.Interviews.Where(a => a.Id == command.Id).FirstOrDefaultAsync();
				if (interview == null)
				{
					return default;
				}

				this._context.Interviews.Remove(interview);
				await this._context.SaveChangesAsync();
				return interview.Id;
			}
		}
	}
}
