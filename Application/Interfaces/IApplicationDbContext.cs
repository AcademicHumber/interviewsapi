﻿namespace Application.Interfaces
{
	using System.Threading.Tasks;
	using Domain.Entities;
	using Microsoft.EntityFrameworkCore;

	/// <summary>
	/// This interface creates the contract to use in order to connect with a database.
	/// </summary>
	public interface IApplicationDbContext
	{
		/// <summary>
		/// Gets or sets the list of interviews.
		/// </summary>
		DbSet<Interview> Interviews { get; set; }

		/// <summary>
		/// This task save the changes on the database.
		/// </summary>
		/// <returns>Success or error message.</returns>
		Task<int> SaveChangesAsync();
	}
}
