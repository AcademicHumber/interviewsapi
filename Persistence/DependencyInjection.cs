﻿namespace Persistence
{
	using Application.Interfaces;
	using Microsoft.EntityFrameworkCore;
	using Microsoft.Extensions.Configuration;
	using Microsoft.Extensions.DependencyInjection;
	using Persistence.Context;

	/// <summary>
	/// Static class to extend IserviceCollection methods.
	/// </summary>
	public static class DependencyInjection
	{
		/// <summary>
		/// Adds the persistence capability with SQL Server.
		/// </summary>
		/// <param name="services">Service instance.</param>
		/// <param name="configuration">Configuration instance.</param>
		public static void AddPersistence(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
				options.UseSqlServer(
					configuration.GetConnectionString("DefaultConnection"),
					b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

			services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
		}
	}
}
