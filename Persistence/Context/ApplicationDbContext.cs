﻿namespace Persistence.Context
{
	using System.Threading.Tasks;
	using Application.Interfaces;
	using Domain.Entities;
	using Microsoft.EntityFrameworkCore;

	/// <summary>
	/// Class to implement the DB context, it inherits from the core layer of the app.
	/// </summary>
	public class ApplicationDbContext : DbContext, IApplicationDbContext
	{
		/// <summary>
		/// Gets or sets the list of interviews.
		/// </summary>
		public DbSet<Interview> Interviews { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
		/// </summary>
		/// <param name="options">Options to initialize the context.</param>
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}

		/// <summary>
		/// Implementation to save the changes on db.
		/// </summary>
		/// <returns>Succes or error response.</returns>
		public async Task<int> SaveChangesAsync()
		{
			return await base.SaveChangesAsync();
		}
	}
}
