﻿
namespace Persistence.Migrations
{
	using Microsoft.EntityFrameworkCore.Migrations;

	/// <summary>
	/// Class.
	/// </summary>
	public partial class Updateinterviews : Migration
	{
		/// <summary>
		/// Up.
		/// </summary>
		/// <param name="migrationBuilder"></param>
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AddColumn<string>(
				name: "Description",
				table: "Interviews",
				nullable: true);
		}

		/// <summary>
		/// Down.
		/// </summary>
		/// <param name="migrationBuilder"></param>
		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropColumn(
				name: "Description",
				table: "Interviews");
		}
	}
}
